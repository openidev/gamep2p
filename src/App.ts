/// <reference path="_reference.ts" />
import Preload = require('./states/Preload');  
import MainMenu = require('./states/MainMenu');  
import Game = require('./states/Game');  

export class Main extends Phaser.Game 
{ 
  socket: any;
  constructor() 
  {
    super(800, 600, Phaser.AUTO, "content");

    this.connectToWebsocket();
    this.state.add("preload", Preload.Preload);
    this.state.add("mainMenu", MainMenu.MainMenu);
    this.state.add("game", Game.Game);
    this.state.start("preload");

    this.socket.on('connect', this.onSocketConnected)
    this.socket.on('disconnect', this.onSocketDisconnect)
  }
  connectToWebsocket()
  {
    let socketAddress = '';
    if (location.hostname !== 'localhost') {
      socketAddress = 'ws://'+location.hostname + ':8000';
    }
    this.socket = io(socketAddress);
  }
  onSocketConnected() {
    console.log('Connected to socket server');
  }
  onSocketDisconnect()
  {
    console.log('Disconnected from socket server');
    this.state.start("mainMenu");
  }

}


var wm: Main;

window.onload = () => {
   wm = new Main();
};