/// <reference path="../_reference.ts" />

export class Player {
  game: Phaser.Game;
  sprite: Phaser.Sprite;
  bullets: Phaser.Group;

  animation: string;
  cursor: any;

  constructor(game: Phaser.Game) {
    this.game = game;
  }

  create(name: string) {
    this.registerSprite(name);
    this.registerBullets(name);

    this.animation = 'idle';
    this.sprite.animations.play(this.animation, null, false);
    this.sprite.events.onAnimationComplete.add(this.updateAnimation, this);

    this.sprite.scale.setTo(0.5, 0.5);
    this.sprite.anchor.setTo(0.5, 0.5);

    this.game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
    this.sprite.body.collideWorldBounds = true;
    this.sprite.body.setSize(159, 159, 20, 20);
    this.sprite.body.maxVelocity.setTo(400, 400);
  }
  registerSprite(name: string)
  {
    this.sprite = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, name, 'idle/survivor-idle_handgun_00');
    this.sprite.animations.add('idle', Phaser.Animation.generateFrameNames('idle/survivor-idle_handgun_', 0, 19, '', 2), 10, true, false);
    this.sprite.animations.add('meleeattack', Phaser.Animation.generateFrameNames('meleeattack/survivor-meleeattack_handgun_', 0, 14, '', 2), 10, true, false);
    this.sprite.animations.add('move', Phaser.Animation.generateFrameNames('move/survivor-move_handgun_', 0, 19, '', 2), 10, true, false);
    this.sprite.animations.add('reload', Phaser.Animation.generateFrameNames('reload/survivor-reload_handgun_', 0, 14, '', 2), 10, true, false);
    this.sprite.animations.add('shoot', Phaser.Animation.generateFrameNames('shoot/survivor-shoot_handgun_', 0, 3, '', 2), 10, true, false);
  }
  registerBullets(name: string) 
  {
    this.bullets = this.game.add.group();
    this.bullets.enableBody = true;
    this.bullets.physicsBodyType = Phaser.Physics.ARCADE;

    this.bullets.createMultiple(50, 'bullet');
    //this.bullets.scale.setTo(0.5, 0.5);
    this.bullets.setAll('checkWorldBounds', true);
    this.bullets.setAll('outOfBoundsKill', true);
  }
  updateAnimation() 
  {
    this.sprite.animations.play(this.animation, null, false);
  }
  update() 
  {
  }
  render()
  {
    if (this.sprite)
    {
      this.game.debug.text('Active Bullets: ' + this.bullets.countLiving() + ' / ' + this.bullets.total, 32, 32);
      //this.game.debug.body(this.sprite);
    }
  }
}


export class LocalPlayer extends Player {

  playerSpeed: number;
  shotTimer: number;
  reloadTimer: number;
  shotButton: any;
  reloadButton: any;

  constructor(game: Phaser.Game) { super(game); }

  create(name: string) {
    super.create(name);
    this.sprite.bringToTop()
    this.playerSpeed = 0;
    this.shotTimer = this.game.time.now;
    this.reloadTimer = 0;
  }

  setInput(cursor:any, shot:any, reload:any)
  {
    this.cursor = cursor;
    this.shotButton = shot;
    this.reloadButton = reload;
  }
  shot()
  {
    console.log('shoot')
    this.animation = 'shot';
    this.shotTimer = this.game.time.now + 150;
    this.sprite.animations.play(this.animation, null, false);

    let bullet = this.bullets.getFirstDead();
    bullet.reset(this.sprite.x, this.sprite.y);
    //this.game.physics.arcade.moveToPointer(bullet, 800);
    bullet.angle = this.sprite.angle + Math.PI;
    this.game.physics.arcade.velocityFromRotation(this.sprite.rotation, 1000, bullet.body.velocity)
  }
  update() {
    let player = this.sprite;

    if (this.shotButton.isDown && this.game.time.now > this.shotTimer) 
    {
      this.shot();
    }
    else if (this.game.time.now > this.reloadTimer)
    {
      this.animation = 'idle';
    }

    if (this.reloadButton.isDown && this.game.time.now > this.reloadTimer) {
      this.animation = 'reload';
      this.reloadTimer = this.game.time.now + 750;
      this.sprite.animations.play(this.animation, null, false);
    }
    else if (this.game.time.now > this.reloadTimer) 
    {
      this.animation = 'idle';
    }

    if (this.cursor.left.isDown) {
      player.angle -= 4
    } else if (this.cursor.right.isDown) {
      player.angle += 4
    }

    if (this.cursor.up.isDown) {
      this.playerSpeed = 300;
      this.animation = 'move';
    } else {
      if (this.playerSpeed > 0) {
        this.playerSpeed -= 4
      }
    }

    this.game.physics.arcade.velocityFromRotation(player.rotation, this.playerSpeed, player.body.velocity)

    if (this.game.input.activePointer.isDown) {
      if (this.game.physics.arcade.distanceToPointer(this.sprite) >= 10) {
        this.playerSpeed = 300
        this.animation = 'move';
        player.rotation = this.game.physics.arcade.angleToPointer(player)
      }
    }

    this.game.socket.emit('move player', { x: player.x, y: player.y, anim: this.animation });
  }
}

export class RemoutePlayer extends Player {
  id: string;
  lastPosition: any;
  lastAnimation: string;

  constructor(game: Phaser.Game) 
  { 
    super(game);
    this.lastPosition = { x: this.game.world.centerX, y: this.game.world.centerY }
  }

  create(name: string) {
    super.create(name);
    this.sprite.body.immovable = true;
    this.sprite.body.collideWorldBounds = true;
    this.sprite.angle = this.game.rnd.angle();
  }
  setData(data: any) 
  {
    this.lastPosition.x = data.x;
    this.lastPosition.y = data.y;
    this.lastAnimation = data.anim;
  }
  destroy() {
    this.sprite.destroy();
    this.id = null;
  }
  update() {
    if (this.id)
    {
      if (this.sprite.x !== this.lastPosition.x || this.sprite.y !== this.lastPosition.y) 
      {
        this.sprite.rotation = this.game.physics.arcade.angleToXY(this.sprite, this.lastPosition.x, this.lastPosition.y);
        this.sprite.x = this.lastPosition.x;
        this.sprite.y = this.lastPosition.y;
      }

      if (this.lastAnimation !== this.animation)
      {
        if (this.lastAnimation !== 'reload')
          this.animation = this.lastAnimation;
        else
          this.sprite.animations.play('reload', null, false);
      }
      this.lastPosition.x = this.sprite.x;
      this.lastPosition.y = this.sprite.y;
    }
  }
}

export class DammPlayer extends Player {

  constructor(game: Phaser.Game) 
  { 
    super(game);
  }

  create(name: string) {
    super.create(name);
  }

  update() {

  }
}