/* ************************************************
** GAME PLAYER CLASS
************************************************ */
var Player = function (startX, startY, animation) {
  var x = startX
  var y = startY
  var anim = animation
  var id

  // Getters and setters
  var getX = function () {
    return x
  }

  var getY = function () {
    return y
  }
  var getAnim = function () {
    return anim
  }

  var setX = function (newX) {
    x = newX
  }

  var setY = function (newY) {
    y = newY
  }
  var setAnim = function (animation) {
    anim = animation
  }

  // Define which variables and methods can be accessed
  return {
    getX: getX,
    getY: getY,
    getAnim: getAnim,
    setX: setX,
    setY: setY,
    setAnim: setAnim,
    id: id
  }
}

// Export the Player class so you can use it in
// other files by using require("Player")
module.exports = Player
