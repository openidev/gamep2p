/// <reference path="../_reference.ts" />

export class MainMenu extends Phaser.State 
{
  button: Phaser.Button;

  constructor() 
  {
    super();
  }
  create()
  {
    this.game.stage.backgroundColor = 0xFFFFF;
    this.button = this.game.add.button(this.game.world.centerX - 95, 400, 'Audi', this.start, this, 2, 1, 0);
    console.log('Socket client id: ' + this.game.socket.id)
  }
  start()
  {
    this.game.state.start('game');
  }
  preload() 
  {
  }
  render() 
  {
  }
  update() 
  {
  }
}
