/// <reference path="../_reference.ts" />

export class Preload extends Phaser.State 
{ 
  loadLine: Phaser.Rectangle;
  lineSize: number;

  constructor() 
  { 
    super(); 
  } 
  preload() 
  { 
    console.log('Preload resource for "Preload" State');
  } 
  create()
  {
    console.log(this.game.world.height);
    this.lineSize = this.game.world.height / 100;

    this.game.stage.backgroundColor = '#182d3b'; //0xFFFFF;
    this.game.load.onLoadStart.add(this.loadStart, this);
    this.game.load.onFileComplete.add(this.fileComplete, this);
    this.game.load.onLoadComplete.add(this.loadComplete, this);

    this.loadLine = new Phaser.Rectangle(0, this.game.world.centerY - this.lineSize / 2,
                                         0, this.lineSize
                                         );

    this.loadResource();

    this.game.load.start();
  }
  render() 
  {
    this.game.debug.rectangle(this.loadLine);
  }
  loadResource()
  {
    this.game.load.image('Audi', "img/cars/Audi.png");
    this.game.load.physics('physicsData', 'physics/cars.json');
    this.game.load.image('earth', 'img/light_sand.png')
    this.game.load.image('bullet', 'img/player/bullet_2.png')
    this.game.load.image('bullet_1', 'img/player/bullet_1.png')
    this.game.load.atlasJSONHash('player-handgun', 'img/player/handgun.png', 'img/player/handgun.json');
  }
  loadStart()
  {
  }
  fileComplete(progress: any, cacheKey: any, success: any, totalLoaded: any, totalFiles: any)
  {
    this.loadLine.width = this.game.world.width * progress/100;
  }
  loadComplete() {
    this.game.state.start('mainMenu');
  }
}
