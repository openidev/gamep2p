/// <reference path="../_reference.ts" />

import Player = require('../actors/Player');  

export class Game extends Phaser.State 
{
  player: Player.LocalPlayer;
  remoutePlayers: Player.RemoutePlayer[];
  land: Phaser.TileSprite;
  cursor:any;

  constructor()
  {
    super(); 
  }
  
  preload()
  {
    this.game.stage.disableVisibilityChange = true;
    this.game.stage.backgroundColor = 0xFFFFF;
  }
  
  create()
  {
    this.game.world.setBounds(0, 0, 1920, 1920);

    this.land = this.game.add.tileSprite(0, 0, 1920, 1920, 'earth');
    this.land.fixedToCamera = true

    this.game.physics.startSystem(Phaser.Physics.ARCADE);

    this.player = new Player.LocalPlayer(this.game);
    this.remoutePlayers = [];

    this.cursor = this.game.input.keyboard.createCursorKeys();
    let shotButton = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    let reloadButton = this.game.input.keyboard.addKey(Phaser.Keyboard.R);
    this.player.setInput(this.cursor, shotButton, reloadButton);

    this.player.create('player-handgun');

    this.game.camera.follow(this.player.sprite)
    this.onEventsRegister();
  }
  onEventsRegister()
  {
    this.game.socket.emit('new player', { x: this.player.sprite.x, y: this.player.sprite.y })
    // New player message received
    this.game.socket.on('new player', (data: any) => this.onNewPlayer(data))
    this.game.socket.on('remove player', (data: any) => this.onRemovePlayer(data))
    this.game.socket.on('move player', (data: any) => this.onUpdatePlayer(data));
  }
  onNewPlayer(data)
  {
    console.log('New player connected:', data.id);
    let removePlayer = this.playerById(data.id);
    if (removePlayer)
      return;
    let player = new Player.RemoutePlayer(this.game);
    this.remoutePlayers.push(player);
    player.create('player-handgun');
    player.id = data.id;
    player.setData(data);
  }
  onRemovePlayer(data) 
  {
    console.log('Player disconnect:', data.id);
    let removePlayer = this.playerById(data.id);
    if (!removePlayer) 
      return

    removePlayer.destroy();
    this.remoutePlayers.splice(this.remoutePlayers.indexOf(removePlayer), 1);
  }
  onUpdatePlayer(data) 
  {
    let player = this.playerById(data.id);
    if (!player)
      return
    player.setData(data);
  }
  update()
  {
    this.player.update();
    for (var i = 0; i < this.remoutePlayers.length; i++)
    {
      this.remoutePlayers[i].update();
    }

    for (var i = 0; i < this.remoutePlayers.length; i++)
    {
      this.game.physics.arcade.collide(this.player.sprite, this.remoutePlayers[i].sprite);
      this.game.physics.arcade.collide(this.player.bullets, this.remoutePlayers[i].bullets);
    }

    this.land.tilePosition.x = -this.game.camera.x;
    this.land.tilePosition.y = -this.game.camera.y;
  }
  render() 
  {
    this.player.render();
    for (var i = 0; i < this.remoutePlayers.length; i++)
    {
      this.remoutePlayers[i].render();
    }
  }
  playerById(id:string) :any
  {
    for (var i = 0; i < this.remoutePlayers.length; i++)
    {
      if (this.remoutePlayers[i].id === id) 
      {
        return this.remoutePlayers[i];
      }
    }
    return false;
  }
}
