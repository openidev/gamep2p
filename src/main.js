var fs = require('fs')
  , http = require('http')
  , ecstatic = require('ecstatic')
  , path = require('path')
  , io = require('socket.io')
  , Player = require('./actors/Player')
  , port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8000
  , ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';

/* ************************************************
** GAME VARIABLES
************************************************ */

var socket  // Socket controller
var players // Array of connected players

/* ************************************************
** GAME INITIALISATION
************************************************ */

var server = http.createServer(
  ecstatic({ root: path.resolve(__dirname, '../build') })
).listen(port, ip, function(err) {
  console.log( "Listening on " + ip + ", server_port " + port )
  if (err) {
    throw err
  }
  initServer();
});

server.on("error", err=>console.log(err));

function initServer () {
  players = []
  socket = io.listen(server)
  setEventHandlers()
}

/* ************************************************
** GAME EVENT HANDLERS
************************************************ */
var setEventHandlers = function () {
  // Socket.IO
  socket.sockets.on('connection', onSocketConnection)
}

function onSocketConnection (client) {
  console.log('New player has connected: ' + client.id)
  client.on('disconnect', onClientDisconnect)

  // Listen for new player message
  client.on('new player', onNewPlayer)

  client.on('move player', onMovePlayer)
}

// Socket client has disconnected
function onClientDisconnect () {
  console.log('Player has disconnected: ' + this.id)

  var removePlayer = playerById(this.id)

  // Player not found
  if (!removePlayer) {
    console.log('Player not found: ' + this.id)
    return
  }

  // Remove player from players array
  players.splice(players.indexOf(removePlayer), 1)

  // Broadcast removed player to connected socket clients
  this.broadcast.emit('remove player', {id: this.id})
}

// New player has joined
function onNewPlayer (data) {
  // Create a new player
  var newPlayer = new Player(data.x, data.y, 'idle')
  newPlayer.id = this.id

  // Broadcast new player to connected socket clients
  this.broadcast.emit('new player', {id: newPlayer.id, x: newPlayer.getX(), y: newPlayer.getY(), anim: newPlayer.getAnim()})

  // Send existing players to the new player
  var i, existingPlayer
  for (i = 0; i < players.length; i++) {
    existingPlayer = players[i]
    this.emit('new player', {id: existingPlayer.id, x: existingPlayer.getX(), y: existingPlayer.getY(), anim: existingPlayer.getAnim()})
  }

  // Add new player to the players array
  players.push(newPlayer)
}

// Player has moved
function onMovePlayer (data) {
  // Find player in array
  var movePlayer = playerById(this.id)

  //console.log(this.id, data.x, data.y)
  // Player not found
  if (!movePlayer) {
    console.log('Player not found: ' + this.id)
    return
  }

  // Update player position
  movePlayer.setX(data.x)
  movePlayer.setY(data.y)
  movePlayer.setAnim(data.anim)

  // Broadcast updated position to connected socket clients
  this.broadcast.emit('move player', {id: movePlayer.id, x: movePlayer.getX(), y: movePlayer.getY(), anim: movePlayer.getAnim()})
}

/* ************************************************
** GAME HELPER FUNCTIONS
************************************************ */
// Find player by ID
function playerById (id) {
  var i
  for (i = 0; i < players.length; i++) {
    if (players[i].id === id) {
      return players[i]
    }
  }

  return false
}