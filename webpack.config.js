var webpack = require('webpack');
var copy = require('copy-webpack-plugin');
var path = require('path');

const PATHS = {
  app: path.join(__dirname, 'src'),
  build: path.join(__dirname, 'build')
};


module.exports ={  
  entry: './src/App.ts',
  output: {
    filename: 'build/js/app.js'
  },
  devtool: 'source-map',
  devServer:{
    contentBase: 'build'
  },
  resolve: {
    extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
  },
  plugins: [
    //new webpack.optimize.UglifyJsPlugin(),
    new copy([
      { from: 'assets/html/index.html', to: './build' },
      { from: 'phaser/build/phaser.min.js', to: './build/js' },
      { from: 'assets/img/', to: './build/img' },
      { from: 'assets/physics/', to: './build/physics' }
    ])
  ],
  module: {
    loaders: [
      { test: /\.ts$/, loader: 'ts' }
    ]
  }
}